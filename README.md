Dans le Meal is a web application to to find cooking recipes, based on your preferences.

This is a group project carried out in **React** during my 4th year of study. The project is available for testing purpose on my portfolio just right [here][1].

How to use it ?
---------------

* The first time you visit the website, Dans le Meal asks you some questions to define your preferences: what are your favorite cuisines? If you have food allergies? And if you follow some diets?
* After this little configuration, you have some recipe suggestions on the home page based on your preferences
* You can also search other recipes by text that you can filter by cuisines, diets and intolerances
* For each recipe, you have a lot of informations: the number of servings, the duration to make the recipe, some nutrition statistics and of course the list of ingredients and steps
* If you want to save a recipe, you can mark it as a favorite 

*All the data (preferences and favorites) is stored locally, in your web browser*

Installation
------------

It is possible for you to test the application by yourself locally. For this, the source files are at your disposal.

You also have a `docker-compose.yml` file allowing you to launch the application in a Docker container on port 8080.

About me
--------

I am a Computer Science Master student. You can find some of my other projects on my [Portfolio][2]. 

[1]: https://danslemeal.cyrilpiejougeac.dev
[2]: https://portfolio.cyrilpiejougeac.dev
